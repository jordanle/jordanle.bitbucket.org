
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

// Google API Keys
var placesKey    = "AIzaSyBptzBsbxK3EzVqxIpT9c9V1mAYLgLgwCk";
var geocodingKey = "AIzaSyBptzBsbxK3EzVqxIpT9c9V1mAYLgLgwCk";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
	
	console.log(ime, priimek, datumRojstva);

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	var sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	console.log(ehrId);

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				console.log(party);
				$("#pridobitev-prikaz").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
	preberiMeritve();
}

function dodajMeritveVitalnihZnakov() {
	var sessionId = getSessionId();

	var ehrId = $("#vnosEhrId").val();
	var datumInUra = $("#vnosDatumInUra").val();
	var telesnaVisina = $("#vnosVisina").val();
	var telesnaTeza = $("#vnosTeza").val();

	var merilec = "Jaz";

	if (!ehrId || ehrId.trim().length == 0) {
		$("#vnosSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			   // Struktura predloge je na voljo na naslednjem spletnem naslovu:
    		  // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#vnosSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#vnosSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}




function izracunajBMI(visina, teza) {
	return teza / ((visina / 100) * (visina / 100));
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 
 
 function generirajPodatke() {
 	generirajPodatke1(1);
 	generirajPodatke1(2);
 	generirajPodatke1(3);
 }
 
 
function generirajPodatke1(stPacienta) {
  
  var ehrId = "-1";
	
  var sessionId = getSessionId();

  var imena = ["Janez", "Borut", "Ancka"];
  var priimki = ["Horpa", "Novak", "Ruzna"];
  var rojstva = ["1970-01-01", "1980-02-02", "1990-03-03"];
  
  var teze = [190, 75, 40];
  var visine = [130, 180, 170];
  
  var datumi = ["2015-10-10", "2015-11-11", "2015-09-10"];
  
  var merilec = "Jaz";
  
  $.ajaxSetup( {
  	headers: {"Ehr-Session": sessionId}
  });
  $.ajax({
  	url: baseUrl + "/ehr",
  	type: 'POST',
  	success: function(data) {
  		ehrId = data.ehrId;
  		var partyData = {
  			firstNames: imena[stPacienta - 1],
  			lastNames: priimki[stPacienta - 1],
  			dateOfBirth: rojstva[stPacienta - 1],
  			partyAdditionalInfo: [{key: "ehrId", value: ehrId}]	
  		};
  	$.ajax({
  		url:baseUrl + "/demographics/party",
  		type: 'POST',
  		contentType: 'application/json',
  		data: JSON.stringify(partyData),
  		success: function(party) {
  			if(party.action == 'CREATE') {
  				
						$.ajaxSetup({
							    headers: {"Ehr-Session": sessionId}
							});
							var podatki = {
							    "ctx/language": "en",
							    "ctx/territory": "SI",
							    "ctx/time": datumi[stPacienta - 1],
							    "vital_signs/height_length/any_event/body_height_length": visine[stPacienta - 1],
							    "vital_signs/body_weight/any_event/body_weight": teze[stPacienta - 1],
							};
							var parametriZahteve = {
							    ehrId: ehrId,
							    templateId: 'Vital Signs',
							    format: 'FLAT',
							    committer: merilec
							};
							$.ajax({
							    url: baseUrl + "/composition?" + $.param(parametriZahteve),
							    type: 'POST',
							    contentType: 'application/json',
							    data: JSON.stringify(podatki),
							    success: function (res) {
							        ;
							    },
							    error: function(err) {
							    	;
							    }
							});
  				
  				$("#generiranID").append(stPacienta + ". Generiran ID: " + ehrId + "<br />");
  			}
  		}
  	})	
  	
  	}
  })


  return ehrId;
}

function preberiMeritve() {
	var sessionId = getSessionId();
	
	var ehrId = $("#preberiEHRid").val();
	var visina = -1;
	var teza = -1;

	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
		success: function(data) {
			console.log("SUCC 1");
			var party = data.party;	
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/" + "weight",
				type: 'GET',
				headers: {"Ehr-Session": sessionId},
				success: function(res) {
					console.log("SUCC 2");
					console.log(res[0].weight);
					teza = res[0].weight;
					
					$.ajax({
						url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
						type: 'GET',
						headers: {"Ehr-Session": sessionId},
						success: function(data) {
							console.log("SUCC 2_1");
							var party = data.party;	
							$.ajax({
								url: baseUrl + "/view/" + ehrId + "/" + "height",
								type: 'GET',
								headers: {"Ehr-Session": sessionId},
								success: function(res) {
									console.log("SUCC 2_2");
									console.log(res[0].height);
									visina = res[0].height;
									
									console.log("TEZA::",teza, "  VISINA::", visina);
									
									var BMI = izracunajBMI(visina, teza);
									
									
									
									
									$("#pridobitev-izpisBMI").html("Telesna teža: " + teza);
									$("#pridobitev-izpisBMI").append("<br />Telesna višina: " + visina);
									$("#pridobitev-izpisBMI").append("<br /><strong>Vaš BMI je: " + BMI + "</strong>");
									
									if(BMI < 18.5) {
											$("#pridobitev-izpisBMI").append("<br />Izgleda, kot da ste podhranjeni. Kaj če bi v spodnje vnosno polje vnesi svoj naslov, \
																			  da vam lahko naš edinstven algoritem svetuje kako naprej?");
									}
									else if(BMI > 25) {
										$("#pridobitev-izpisBMI").append("<br />Izgleda, kot da ste preobilni. Kaj če bi v spodnje vnosno polje vnesi svoj naslov, \
																			  da vam lahko naš edinstven algoritem svetuje kako naprej?");
										
									}
									else {
										$("#pridobitev-izpisBMI").append("<br />Izgleda, kot da ste zdrave telesne teže, ampak bi se kljub temu lahko še malo bolj razgibali. \
																				Kaj če bi v spodnje vnosno polje vnesi svoj naslov, \
																				da vam lahko naš edinstven algoritem svetuje kako naprej?");
									}
									
								}
							});
						}
					});
					
					
				}
			});
		}
	});

}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function prikaziDodajanjePacienta() {
	$("#pridobitev-podatkov").hide();
	$("#vnos-podatkov").hide();
	$("#pridobitev-podatkov-btn").attr('class', 'btn btn-default');
	$("#vnos-podatkov-btn").attr('class', 'btn btn-default');
	
	$("#dodajanje-pacienta").show();
	$("#dodajanje-pacienta-btn").attr('class', 'btn btn-info');
}

function prikaziPridobitevPodatkov() {
	$("#dodajanje-pacienta").hide();
	$("#vnos-podatkov").hide();
	
	$("#dodajanje-pacienta-btn").attr('class', 'btn btn-default');
	$("#vnos-podatkov-btn").attr('class', 'btn btn-default');
	
	$("#pridobitev-podatkov").show();
	$("#pridobitev-podatkov-btn").attr('class', 'btn btn-info');
}

function prikaziVnosPodatkov() {
	$("#pridobitev-podatkov").hide();
	$("#dodajanje-pacienta").hide();
	$("#dodajanje-pacienta-btn").attr('class', 'btn btn-default');
	$("#pridobitev-podatkov-btn").attr('class', 'btn btn-default');
	
	$("#vnos-podatkov").show();
	$("#vnos-podatkov-btn").attr('class', 'btn btn-info');
}

// Vrne: latitude,longitude skozi callback
function pridobiKoordinate(naslov, callback) {
	var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + naslov + "&key=" + geocodingKey;
	
	$("#pridobitev-sporocilo").html("Če se povezava ne pojavi, osvežite stran in poskusite znova (kriv je Google)");
	
	$.getJSON(url, function(data) {
		if(data.results !== undefined && data.results[0] !== undefined) {
			var geometry = data.results[0].geometry;
			
			if(geometry !== undefined) {
				var location = geometry.location;
				
				callback(location.lat.toString() + "," + location.lng.toString());
			}
		}
	});
}

function povezavaDoRestavracij(location) {
	var link = "https://www.google.com/maps/search/restaurants/@" + location;
	
	return link;
}

function povezavaDoFitnessCentrov(location) {
	var link = "https://www.google.com/maps/search/fitness+center/@" + location;
	
	return link;
}


function pocakajLokacijo(location) {
	
		var restavracijeLink = povezavaDoRestavracij(location);
		var fitnesiLink = povezavaDoFitnessCentrov(location);
		
      console.log("Bližnje restavracije so na voljo na povezavi: " + restavracijeLink);
      
      $("#pridobitev-sporocilo").append("Bližnje restavracije so na voljo na povezavi: " + restavracijeLink);
      $("#pridobitev-sporocilo").append("<br />Bližnji fitness centri so na voljo na povezavi: " + fitnesiLink);
      
      console.log("Bližnji fitness centri so na voljo na povezavi: " + fitnesiLink);
    }
    
function prikaziLokacije() {
	var lokacija = $("#lokacija").val();
	pridobiKoordinate(lokacija, pocakajLokacijo);
}

